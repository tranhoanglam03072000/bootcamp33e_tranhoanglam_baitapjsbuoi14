/*======================Bài 1=================================*/
document.getElementById("btnketqua1").onclick = function () {
  //input : nhapsomot,nhapsohai,nhapsoba : number
  var nhapSoMot = Number(document.getElementById("nhapsomot").value);
  var nhapSoHai = Number(document.getElementById("nhapsohai").value);
  var nhapSoBa = Number(document.getElementById("nhapsoba").value);
  //output: ketqua1:string
  var ketQua1 = "";
  var soThuMot = 0;
  var soThuHai = 0;
  var soThuBa = 0;
  //progress
  if (nhapSoMot <= nhapSoHai && nhapSoHai < nhapSoBa) {
    soThuMot = nhapSoMot;
    soThuHai = nhapSoHai;
    soThuBa = nhapSoBa;
  } else if (nhapSoMot < nhapSoBa && nhapSoBa <= nhapSoHai) {
    soThuMot = nhapSoMot;
    soThuHai = nhapSoBa;
    soThuBa = nhapSoHai;
  } else if (nhapSoHai < nhapSoMot && nhapSoMot <= nhapSoBa) {
    soThuMot = nhapSoHai;
    soThuHai = nhapSoMot;
    soThuBa = nhapSoBa;
  } else if (nhapSoHai <= nhapSoBa && nhapSoBa < nhapSoMot) {
    soThuMot = nhapSoHai;
    soThuHai = nhapSoBa;
    soThuBa = nhapSoMot;
  } else if (nhapSoBa <= nhapSoMot && nhapSoMot < nhapSoHai) {
    soThuMot = nhapSoBa;
    soThuHai = nhapSoMot;
    soThuBa = nhapSoHai;
  } else if (nhapSoBa < nhapSoHai && nhapSoHai <= nhapSoMot) {
    soThuMot = nhapSoBa;
    soThuHai = nhapSoHai;
    soThuBa = nhapSoMot;
  }

  if (nhapSoMot == nhapSoHai && nhapSoHai == nhapSoBa) {
    ketQua1 =
      "kết quả sắp xếp là : " +
      nhapSoMot +
      " = " +
      nhapSoHai +
      " = " +
      nhapSoBa;
  } else if (soThuMot == soThuHai && soThuHai < soThuBa) {
    ketQua1 =
      "kết quả sắp xếp là : " + soThuMot + " = " + soThuHai + " < " + soThuBa;
  } else if (soThuMot < soThuHai && soThuHai == soThuBa) {
    ketQua1 =
      "kết quả sắp xếp là : " + soThuMot + " < " + soThuHai + " = " + soThuBa;
  } else {
    ketQua1 =
      "kết quả sắp xếp là : " + soThuMot + " < " + soThuHai + " < " + soThuBa;
  }
  document.getElementById("ketqua1").innerHTML = ketQua1;
};

/*===========================Bài 2==============================*/
document.getElementById("btnxacnhan").onclick = function () {
  //input: 1=bố, 2 =mẹ , 3= anh trai, 4 = em gái =>>> number
  var who = Number(document.getElementById("who").value);
  console.log(who);
  //output: ketqua:string
  var ketQua2 = "";
  //progress
  if (who === 1) {
    ketQua2 = " Xin chào papa thân iuuu <3";
  } else if (who === 2) {
    ketQua2 = " Xin chào mama yêu quýyyyyy <3";
  } else if (who === 3) {
    ketQua2 = "Xin chào anh zai";
  } else {
    ketQua2 = "Xin chào em gái";
  }
  document.getElementById("ketqua2").innerHTML = ketQua2;
};

/*=============================Bài 3========================= */
document.getElementById("btnketqua3").onclick = function () {
  //input: nhapso1,nhapso2,nhapso3: number
  var nhapSo1 = Number(document.getElementById("nhapso1").value);
  var nhapSo2 = Number(document.getElementById("nhapso2").value);
  var nhapSo3 = Number(document.getElementById("nhapso3").value);
  //output: ketqua : string
  var ketQua3 = "";
  //progress
  // gán số  lẽ
  var soMotLe = 0;
  var soHaiLe = 0;
  var soBaLe = 0;
  //Gán số thứ tự
  var tongSoLe = null;
  var tongSoChan = null;
  if (nhapSo1 % 2 !== 0) {
    soMotLe = 1;
  } else {
    soMotLe = 0;
  }
  if (nhapSo2 % 2 !== 0) {
    soHaiLe = 1;
  } else {
    soHaiLe = 0;
  }
  if (nhapSo3 % 2 !== 0) {
    soBaLe = 1;
  } else {
    soBaLe = 0;
  }
  soMotLe = Number(soMotLe);
  soHaiLe = Number(soHaiLe);
  soBaLe = Number(soBaLe);
  tongSoLe = soBaLe + soMotLe + soHaiLe;
  tongSoChan = 3 - tongSoLe;
  document.getElementById("ketqua3").innerHTML =
    " Vậy có tổng cộng : " +
    tongSoLe +
    " số lẻ và " +
    tongSoChan +
    " số chẵn .";
};
/*===========================Bài 4 ========================== */
document.getElementById("btnketqua4").onclick = function () {
  // input nhapcanha, nhapcanhb, nhapcanhc
  var nhapCanhA = Number(document.getElementById("nhapcanha").value);
  var nhapCanhB = Number(document.getElementById("nhapcanhb").value);
  var nhapCanhC = Number(document.getElementById("nhapcanhc").value);
  //output: ketqua:string
  var ketQua4 = "";
  //progress
  // điều kiện của 1 tam giác là 1 cạnh bất kì phải bé hơn tổng của 2 cạnh còn lại vd: a < b + c , a,b,c > 0
  var dieuKien =
    nhapCanhC < nhapCanhA + nhapCanhB &&
    nhapCanhB < nhapCanhA + nhapCanhC &&
    nhapCanhA < nhapCanhB + nhapCanhC &&
    nhapCanhA > 0 &&
    nhapCanhB > 0 &&
    nhapCanhC > 0;

  // điều kiện của 1 tam giác vuông : bình phương cạnh huyền = tổng bình phương 2 cạnh còn lại vd: a*a = b*b + c*c
  var dieuKienTamGiacVuong =
    nhapCanhA * nhapCanhA == nhapCanhB * nhapCanhB + nhapCanhC * nhapCanhC ||
    nhapCanhB * nhapCanhB == nhapCanhA * nhapCanhA + nhapCanhC * nhapCanhC ||
    nhapCanhC * nhapCanhC == nhapCanhB * nhapCanhB + nhapCanhA * nhapCanhA;

  //điều kiện 1 tam giác cân là 2 cạnh của 1 tam giác bằng nhau
  var dieuKienTamGiacCan =
    nhapCanhA == nhapCanhB || nhapCanhA == nhapCanhC || nhapCanhB == nhapCanhC;
  // điều kiện của 1 tam giác đều là 3 cạnh bằng nhau : a =b =c
  var dieuKienTamGiacDeu = nhapCanhA == nhapCanhB && nhapCanhB == nhapCanhC;
  //điều kiện là tam giác tù: a*a > b*b + c*c
  var dieuKienTamGiacTu =
    nhapCanhA * nhapCanhA > nhapCanhB * nhapCanhB + nhapCanhC * nhapCanhC;
  // điều kiện là 1 tam giác vuông cân: vừa là tam giác đều và vừa là tam giác cân
  var dieuKienTamGiacVuongCan = dieuKienTamGiacCan && dieuKienTamGiacVuong;
  if (dieuKien) {
    if (dieuKienTamGiacVuong) {
      ketQua4 = " Là tam giác vuông";
    } else if (dieuKienTamGiacDeu) {
      ketQua4 = " là tam giác Đều";
    } else if (dieuKienTamGiacCan) {
      ketQua4 = " là tam giác Cân";
    } else if (dieuKienTamGiacTu) {
      ketQua4 = "là tam giác tù";
    } else if (dieuKienTamGiacVuongCan) {
      ketQua4 = " là tam giác vuông cân";
    } else {
      ketQua4 = " là tam giác nhọn";
    }
  } else {
    ketQua4 = "hình như cái này không phải là tam giác bạn êyyy";
  }
  document.getElementById("ketqua4").innerHTML = ketQua4;
};
